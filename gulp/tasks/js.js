import webpack from 'webpack-stream';
import jsClean from 'gulp-js-minify';
import rename from 'gulp-rename';

export const js = () => {
  return (
    app.gulp
      .src(app.path.src.js, { sourcemaps: false })
      .pipe(
        app.plugins.plumber(
          app.plugins.notify.onError({
            title: 'JS',
            message: 'Error: <=% error.message %>',
          })
        )
      )
      .pipe(app.plugins.concat('scripts.js'))
      .pipe(jsClean())
      .pipe(
        rename({
          extname: '.min.js',
        })
      )
      // .pipe(
      //   webpack({
      //     mode: app.isBuild ? 'production' : 'development',
      //     output: {
      //       filename: 'app.min.js',
      //     },
      //   })
      // )
      .pipe(app.gulp.dest(app.path.build.js))
      .pipe(app.plugins.browsersync.stream())
  );
};
